While mobile software increases in complexity and hardware increases in performance, battery capacity lags behind.
In practice this means that playing games, watching movies or just browsing the Web will drain the battery within a few hours.

There are mainly two ways to tackle this problem: increase battery capacity or consume less power.
While there is research being done on improving batteries, it is still not ready for the mainstream market~\cite{Batteries}.
When it eventually reaches phone manufacturers it will take time for the majority of people to adopt these new and expensive phones.
In addition, adding more battery capacity will not necessarily increase the duration the phone can be used.
Software and hardware will likely utilize this new power to increase precision, speed, quality and add more effects.
Thus, the battery might not last any longer.
With these facts in mind, consuming less power should be a more sustainable, feasible and faster way of increasing the duration a phone can be used before running out of power.

Battery power is consumed all the time while a phone is on, but it consumes an increased amount of power while its screen is actively used rather than being stored in a pocket or just laying on a table.
Some changes have already been implemented to increase how long the battery lasts while the screen actively used (the screen-on time).
Several phone manufacturers include battery saving modes which decrease the frame rate (number of image frames rendered per second), clock down the CPU and GPU frequencies, dim the screen and disable animations.
While these changes do increase the screen-on time, the user experience is compromised.
Studies show that the frame rate has a significant effect on playability~\cite{Claypool,JanzenTeather}.
This suggests that decreasing the frame rate would likely compromise the user experience, especially in games.
Thus, the question is: \textit{how can we optimize screen-on time for a user while not compromising the experience?}

When the phone screen is on, the GPU has to render content to it.
The GPU processing pipeline is complex, but there are two especially important parts: vertex processing and fragment processing.
The former is resolution independent, but the latter requires a computation per pixel (fragment).
Thus, the higher the screen resolution is, the more computations will be executed, independent of the scene complexity, leading to a higher power consumption.
This effect is even more apparent in games because they often compute the same pixel in multiple iterations and do complex computations on them.

This is an especially important issue because phones sold since 2013 have a DPI (dots per inch) higher than a user can perceive with the naked eye, and this DPI only increases per year.
Samsung is especially pushing for a higher DPI, with some of the reason being their Gear VR headset~\cite{GearVr}.
Getting a good VR (virtual reality) experience requires a high DPI because of the close proximity to the eye.
As an example of the change in DPI, Samsung Galaxy S4 (2013) has 441 DPI and the Samsung Galaxy S6 (2015) has 577 DPI~\cite{GalaxyS4,GalaxyS6}.
A larger DPI on a screen of the same physical size means more pixels and thus a higher GPU processing cost.
In most cases, parts of this GPU processing will be unnecessary as the human eye can not perceive such a high DPI.
The further away the screen is from the user (user-screen distance), the less detail a user can perceive.
Thus, by measuring the user-screen distance, one can calculate the minimum DPI required for a human to not see the individual pixels and set the resolution to match this.
This is what Songtao He, Yunxin Liu and Hucheng Zhou's paper discussed and implemented a solution for~\cite{HeLiuZhou}.

They approach the problem by implementing a DRS (dynamic resolution scaling) system in two 2015-model Android phones that dynamically changes the render target resolution based on ultrasonic-measured user-screen distance.
The further away the user is, the lower the resolution of the phone will be.
It is important to note that this does not change the physical amount of pixels on the device, it only changes how many pixels the GPU needs to render and then upscales the image to match the physical screen resolution.

The rendering of pixels on the GPU is managed by the OpenGL ES graphics API on Android.
To do the scaling, they intercept certain calls in these APIs and change the resolution parameters before calling the actual OpenGL ES API which talks to the hardware.
This could potentially be replicated in any graphics API such as Microsoft's DirectX and Apple's Metal, not only OpenGL ES. But the open nature of OpenGL ES and Android makes it easier to implement and allows the interceptions to be implemented directly on Android phones without changing the system OS.
Their interception method introduces less than 1 ms extra latency, and since a frame has 16.67 ms (60 frames per second), this latency should not affect the user experience.

To test if the DRS will affect the user experience, they conduct a small user study (10 users).
None of the users detected the change in resolution, even though it changed 136 times on average.
Their power consumption measurements show that \say{[t]he energy per frame can be reduced by 30.1\% on average and up to 60.5\% at most when the resolution is halved, for 15 apps.}
The apps they tested were games, but they also saw a 10.2\% average improvement for three non-game apps.
This means that, depending on the game or app, the default screen resolution and the user-screen distance, it is possible to play mobile games for 2.5 times as long as usual without compromising user experience.
E.g.~if you normally could play games for 4 hours, you can now play games for 10 hours, which is a significant improvement.
For the average game power consumption you could play about 1.4 times longer than usual, which also is a good improvement.
These two findings suggest that using a user-screen distance based DRS system can significantly improve screen-on time while not compromising user experience.

This is not limited to mobile phones.
Android Wear was first released in 2014 and it brought forward a series of smart watches~\cite{WearAnno}. These also use a GPU and could benefit from the same improvements. With the circular displays introduced by the Moto 360 watch~\cite{Moto360}, combining the DRS improvement with Miao and Lin's suggested circular rendering improvement could decrease the power consumption even more~\cite{MiaoLin}.

What is great about the DRS implementation is that it will in theory work on any Android device without changing the OS.
What is troublesome is that you need to tack on a clumsy and big ultrasonic sensor.
So while the paper shows that it is possible to achieve these power savings with current technology, it is not something that can be used by the consumer market at this moment.
What this paper contributes though, is a proof-of-concept that this kind of technology can save a significant amount of power, especially with the increasing screen resolution on new phone models announced.
This research might push other researchers, manufacturers and developers to delve deeper into this problem and find ways to integrate ultrasonic sensors into phones.
With all the innovative ways that accelerometers, gyroscopes and other sensors have been applied, who knows what an ultrasonic sensor might lead to in addition to improved battery performance?
